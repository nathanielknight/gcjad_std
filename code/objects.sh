#!/bin/bash

if [ -z $2 ]; then
cp Objects.default lbfgs
else if [ $2 = "LBFGS" ]; then

find="inv_hessian_mod.o"

replace="inv_hessian_mod.o             \
inv_hessian_lbfgs_mod.o"

sed -e "s/$find/$replace/g" Objects.default > lbfgs
fi
fi


if [ -z $1 ]; then
    echo "Provide an option (DEFAULT, HDF, LIDORT, SAT_NETCDF, HDF)"
fi


if [ $1 = "HDF" ]; then

find="getifsun.o"
replace="getifsun.o                    \
gvchsq.o "

sed -e "s/$find/$replace/g" lbfgs > output1

find="input_mod.o"
replace="input_mod.o            \
He4IncludeModule.o            \
He4ErrorModule.o              \
He4GridModule.o               \
He4SwathModule.o              \
findinv.o                     \
airsv5_mod.o                  \
airs_co_obs_mod.o             \
HdfIncludeModule.o            \
HdfSdModule.o                 \
HdfVdModule.o                 \
interp.o                      \
gaussj.o                      \
mopitt_obs_mod.o"

sed -e "s/$find/$replace/g" output1 > output
rm output1
mv output Objects.mk
fi

if [ $1 = "SAT_NETCDF" ]; then

find="rpmares_mod.o"
replace="rpmares_mod.o                 \
gosat_co2_mod.o               \
tes_nh3_mod.o                 \
tes_o3_mod.o                  \
tes_o3_irk_mod.o"

sed -e "s/$find/$replace/g" lbfgs > output1

find="tes_ch4_mod.o"
replace="tes_ch4_mod.o                 \
scia_ch4_mod.o"

sed -e "s/$find/$replace/g" output1 > output
rm output1
mv output Objects.mk
fi

if [ $1 = "LIDORT" ]; then

find="population_mod.o"
replace="population_mod.o              \
mie_mod.o                     \
lidort_mod.o"

sed -e "s/$find/$replace/g" lbfgs > output
mv output Objects.mk
fi

if [ $1 = "DEFAULT" ]; then
cp lbfgs Objects.mk
fi

rm lbfgs
