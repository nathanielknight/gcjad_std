      MODULE IMPRV_PROFILE_MOD
      
      USE TRACERID_MOD, ONLY: IDTBCPI, IDTBCPO, IDTOCPI, IDTOCPO
      USE TIME_MOD, ONLY: GET_NYMDB, GET_NYMDE
#       include  "CMN_SIZE"     !--fetches IIPAR, JJPAR (size of grid)

!     #undef imprv_logging      
!     #define imprv_logging  1
#undef imprv_finite_diff
#define imprv_finite_diff  1

!Parameters and Settings-------------------------------------
!--filenames
      CHARACTER :: bc_profile_filename*128 = "bc_profile"
      CHARACTER :: oc_profile_filename*128 = "oc_profile"
      CHARACTER :: indices_filename*128 = "profile_indices"
      CHARACTER :: temp_directory*128 = "profile_tmp/"
!--dates (for memory allocation & access)
      INTEGER :: START_DATE, END_DATE !set from run start/end in imprv_profile_init
      INTEGER :: num_years, num_active_cells !inferred from run dates & profiles
      
!Module level globals----------------------------------------
!--   need to be accessed/shared by many subroutines
      INTEGER, ALLOCATABLE :: profile_indices(:,:)
      INTEGER :: YEAR_OFFSET
      REAL*4 :: IMPRV_ABS_ERR = 2.0
      REAL*4, ALLOCATABLE, DIMENSION(:) :: avg_bcpi, avg_bcpo !running daily average aerosol concentration
      REAL*4, ALLOCATABLE, DIMENSION(:) :: avg_ocpi, avg_ocpo
      REAL*4, ALLOCATABLE, DIMENSION(:) :: cost_func_by_cell !for finite difference test 
      REAL*4, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: imprv_bc_profile
      REAL*4, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: imprv_oc_profile
      REAL*4, ALLOCATABLE, DIMENSION(:) :: dCF
!improve_*_profile contains arrays of imprv data indexed by   
! *c_value = (k,d,m,y)
!where k indicates the active_cell the bc_val comes from and
!d,m,y are the date.
      REAL*4, ALLOCATABLE, DIMENSION(:,:) :: imprv_bc_data !for doing cost function calcs
      REAL*4, ALLOCATABLE, DIMENSION(:,:) :: imprv_oc_data
      REAL*4, ALLOCATABLE, DIMENSION(:,:) :: tmp_imprv_data
      
      LOGICAL :: exists_imprv_data = .false.
      LOGICAL :: exists_bc_data = .false.
      LOGICAL :: exists_oc_data = .false.
      
      CONTAINS
      
!Initialization and Infrastructure====================================
      
      SUBROUTINE IMPRV_PROFILE_INIT()
      USE TIME_MOD, ONLY: GET_TS_CHEM, GET_TS_CONV, GET_TS_DIAG,
     $     GET_TS_DYN, GET_TS_EMIS, GET_TS_UNIT
      
      IMPLICIT NONE
      
      print*, "IMPRV>> Performing imprv initialization."
      START_DATE = GET_NYMDB()
      END_DATE = GET_NYMDE()
      print*, "IMPRV>> START_DATE = ", start_date
      print*, "IMPRV>> END_DATE = ", end_date
      print*, "IMPRV>> Printing Time Steps:"
      print*, "TS_CHEM = ", GET_TS_CHEM()
      print*, "TS_CONV ", GET_TS_CONV()
      print*, "TS_DIAG ", GET_TS_DIAG()
      print*, "TS_DYN ", GET_TS_DYN()
      print*, "TS_EMIS ", GET_TS_EMIS()
      print*, "TS_UNIT ", GET_TS_UNIT()
      
!     derived parameters
      NUM_YEARS = (START_DATE/10000) - (END_DATE/10000) + 1
      YEAR_OFFSET = START_DATE/10000 - 1
      CALL read_profile_indices(indices_filename, profile_indices)
      CALL read_profile(bc_profile_filename, imprv_bc_profile)
      CALL read_profile(oc_profile_filename, imprv_oc_profile)
      
      
!     Initialize arrays for tracking carbon aerosol averages
      ALLOCATE(avg_bcpi(num_active_cells))
      ALLOCATE(avg_bcpo(num_active_cells))
      ALLOCATE(avg_ocpi(num_active_cells))
      ALLOCATE(avg_ocpo(num_active_cells))
      ALLOCATE(dCF(num_active_cells))
      avg_bcpi = 0.0d0
      avg_bcpo = 0.0d0
      avg_ocpi = 0.0d0
      avg_ocpo = 0.0d0
      dCF = 0.0d0
      
!Initialize arrays for calculating cost function
      allocate(imprv_bc_data(2,num_active_cells))
      allocate(imprv_oc_data(2,num_active_cells))
      allocate(tmp_imprv_data(2,num_active_cells))
      
#ifdef imprv_finite_diff
!Initialize cost func by cell (for finite difference test)
      print*, "IMPRV>> Initializing cost_func_by_cell"
      allocate(cost_func_by_cell(num_active_cells))
      cost_func_by_cell = 0.0
#endif
      
      print*, "IMPRV>> Done imprv_profile_init"
!}}}
      END SUBROUTINE imprv_profile_init
      
      
      SUBROUTINE READ_PROFILE_INDICES(FILENAME, PROFILE_INDICES)
!Load data froma file containing the indices of gridboxes with
!IMPROVE observations (and determine how many exist).  {{{
      IMPLICIT NONE
      CHARACTER, INTENT(IN) ::filename*64
      INTEGER, ALLOCATABLE, DIMENSION(:,:), INTENT(OUT) ::
     &     profile_indices
      INTEGER :: nlines=0, handle=213, i
      
      PRINT*, "IMPRV>> Reading indices from ", TRIM(filename)
!Count lines
      OPEN(handle, FILE=filename)
      DO
         READ(handle,*,END=100)
         nlines = nlines + 1
      END DO
 100  CLOSE(handle)
      print*, "Reading ", nlines, " profile indieces from ", 
     &     trim(filename)
      
      num_active_cells = nlines
      
!Allocate an array and read data
      IF (ALLOCATED(profile_indices)) DEALLOCATE(profile_indices)
      ALLOCATE(profile_indices(2,nlines))
      OPEN(handle, FILE=filename)
      print*, "k     i,j"
      DO i=1,nlines
         READ(handle,*) profile_indices(:,i)
         WRITE(*, "(I4.3,I4.3,I4.3)"), i, profile_indices(:,i)
      END DO
      CLOSE(handle)
      
      print*, "IMPRV>> Done read_profile_indices:"
      print*, profile_indices
!}}}
      END SUBROUTINE READ_PROFILE_INDICES
      
      
      
      SUBROUTINE READ_PROFILE(FILENAME, PROFILE)
!{{{
      USE ERROR_MOD, ONLY: ERROR_STOP
      IMPLICIT NONE
      CHARACTER, INTENT(IN) :: FILENAME*64
      REAL*4, ALLOCATABLE, DIMENSION(:,:,:,:,:), INTENT(OUT) ::
     &     PROFILE
      
      INTEGER :: NLINES=0, I, HANDLE=213, NYEARS
      INTEGER :: II, JJ, YYYYMMDD, K, Y, M, D
      REAL :: PROFILE_VALUE, PROFILE_ERROR
      
      PRINT*, "IMPRV>> Starting read_profile . . . "
      PRINT*, "IMPRV>>   Allocating profile memory"
      
      NYEARS = (END_DATE/10000) - (START_DATE/10000) + 1
      IF (ALLOCATED(PROFILE)) DEALLOCATE(PROFILE)
      ALLOCATE(PROFILE(2,NUM_ACTIVE_CELLS,31,12,NYEARS))
      PROFILE = -1.0
      
      PRINT*, "IMPRV>>   Reading profile from ", TRIM(FILENAME)
!--READ INTO AN ARRAY
!--PROFILE HAS FORM "YYYYMMDD *C_VAL *C_ERR I J"
      OPEN(HANDLE, FILE=FILENAME)
      PRINT*, "IMPRV>>   Matching profile coords to profile index"
      DO
         READ(HANDLE,*,END=100) YYYYMMDD, PROFILE_VALUE, 
     &        PROFILE_ERROR , II, JJ
!DETERMINE K
         K=0
         DO I=1,NUM_ACTIVE_CELLS
            IF (II == PROFILE_INDICES(1,I)) THEN
               IF (JJ == PROFILE_INDICES(2,I)) THEN
                  K = I
!PRINT*, PROFILE_INDICES(1,I), PROFILE_INDICES(2,I), "==", II, JJ
               END IF
            END IF
         END DO
         IF (K==0) THEN
            PRINT*, "WHILE PLACING DATA FROM ", II, JJ
            CALL ERROR_STOP(
     &           "IMPROVE DATA WITHOUT  VALID GEOS-CHEM INDEX",
     &           "IMPRV_PROFILE_MOD, READ_PROFILE")
         END IF
         
         Y = YYYYMMDD / 10000
         M = MOD(YYYYMMDD,10000) / 100
         D = MOD(YYYYMMDD,100)
!PRINT*, "IMPRV>> DATA FROM ", YYYYMMDD
!PRINT*, "IMPRV>> ASSIGNED TO", Y, M, D, "WITH OFFSET", YEAR_OFFSET
         PROFILE(1,K,D,M,Y-YEAR_OFFSET) = PROFILE_VALUE
         PROFILE(2,K,D,M,Y-YEAR_OFFSET) = PROFILE_ERROR
      END DO
 100  CLOSE(handle)
      print*, "IMPRV>>    read profile from: ", filename
      print*, profile
!}}}
      END SUBROUTINE READ_PROFILE


      SUBROUTINE GET_IMPRV_BC_DATA(YYYYMMDD, BC_DATA)
!{{{
      INTEGER, INTENT(IN) :: YYYYMMDD
      INTEGER :: D,M,Y
      REAL*4, DIMENSION(:,:), ALLOCATABLE :: BC_DATA
      
      
      BC_DATA = 0.D0
      
!EXTRACT DAY, MONTH, YEAR FROM YYYYMMDD
      Y = YYYYMMDD / 10000
      M = MOD(YYYYMMDD, 10000) / 100
      D = MOD(YYYYMMDD, 100)
      PRINT*, "IMPRV>> GET_BC_DATA @ ", D, M, (Y-YEAR_OFFSET)
      BC_DATA = IMPRV_BC_PROFILE(:,:, D, M, Y-YEAR_OFFSET)
      PRINT*, "IMPRV>> GOT BC DATA:", BC_DATA
!}}}
      END SUBROUTINE GET_IMPRV_BC_DATA
      
      
      SUBROUTINE GET_IMPRV_OC_DATA(YYYYMMDD, OC_DATA)
!{{{
      INTEGER, INTENT(IN) :: YYYYMMDD
      INTEGER :: D,M,Y
      REAL*4, DIMENSION(:,:), ALLOCATABLE :: OC_DATA
      
      OC_DATA = 0.D0
      
!EXTRACT DAY, MONTH, YEAR FROM YYYYMMDD
      Y = YYYYMMDD / 10000
      M = MOD(YYYYMMDD, 10000) / 100
      D = MOD(YYYYMMDD, 100)
      PRINT*, "IMPRV>> GET_OC_DATA @ ", D, M, (Y-YEAR_OFFSET)
      OC_DATA = IMPRV_OC_PROFILE(:,:,D,M,Y-YEAR_OFFSET)
      PRINT*, "IMPRV>> GOT OC DATA:", OC_DATA
!}}}
      END SUBROUTINE GET_IMPRV_OC_DATA
      
      
      
      
      
!During Forward=======================================================
      
      
      SUBROUTINE IMPRV_PROFILE_FORWARD(BCPI, BCPO, OCPI, OCPO)
!forward logic and control
!{{{
!See modified/geos_chem_mod.f for what the *CP* are.
      
      USE TIME_MOD, ONLY: GET_TS_CHEM, GET_HOUR, GET_MINUTE
      USE TIME_MOD, ONLY: GET_NYMD, EXPAND_DATE, GET_NHMS
      
      
      
!IGLOB and JGLOB from CMN_SIZE
! *CP* get passed stright through
      REAL*8,  INTENT(IN)   :: BCPI(IIPAR,JJPAR)
      REAL*8,  INTENT(IN)   :: BCPO(IIPAR,JJPAR)
      REAL*8,  INTENT(IN)   :: OCPI(IIPAR,JJPAR)
      REAL*8,  INTENT(IN)   :: OCPO(IIPAR,JJPAR)
      
      INTEGER :: MINS_TO_MIDNIGHT, MINS_SINCE_MIDNIGHT
      INTEGER :: THRESHOLD, tmp_NYMD
      
#ifdef imprv_logging
!     CHARACTER(LEN=64) ::  logfilename
!     INTEGER :: i,j,k,logfile_unit
#endif
      
      
      print*, "IMPRV>> Starting imprv_profile_forward @"
      print*, "IMPRV>>   ", get_nymd(), get_hour(), get_minute()

!     Checked to make sure, but mins_to and mins_since does add up to
!     one day.

      MINS_TO_MIDNIGHT = 60*(23-GET_HOUR()) + (60-GET_MINUTE())
      MINS_SINCE_MIDNIGHT = 60*GET_HOUR() + GET_MINUTE()
      THRESHOLD = GET_TS_CHEM()
            
!     Iff we're starting a new day, check for IMPROVE data
      IF (MINS_SINCE_MIDNIGHT .lt. THRESHOLD) THEN
         print*, "IMPRV>> Checking for IMPRV profile data."
         exists_bc_data = .false.
         exists_oc_data = .false.
         CALL get_imprv_bc_data(GET_NYMD(), tmp_imprv_data)
         IF (any(tmp_imprv_data>0)) exists_bc_data = .true.
         CALL get_imprv_oc_data(GET_NYMD(), tmp_imprv_data)
         IF (any(tmp_imprv_data>0)) exists_oc_data = .true.
         EXISTS_IMPRV_DATA = (EXISTS_BC_DATA .OR. EXISTS_OC_DATA)

         IF (exists_imprv_data) THEN
            print*, "IMPRV>> Collecting aeroavg data."
         ELSE
            print*, "IMPRV>> Not collecting aeroavg data."
         END IF
      END IF
            
!Iff there's improve data, track average aerosol concentration
      IF (EXISTS_IMPRV_DATA) THEN
         IF (MINS_TO_MIDNIGHT .gt. THRESHOLD) THEN 
!if it's a timestep in the middle of a day, 
            print*, "IMPRV>> Updating aeroavg."
            CALL update_aeroavg(BCPI, BCPO, OCPI, OCPO)
         ELSE
!otherwise, we're ending the day
            print*, "IMPRV>> Writing aeroavg file"
            tmp_NYMD = GET_NYMD()
            CALL update_aeroavg(BCPI, BCPO, OCPI, OCPO)
            CALL write_aeroavg(tmp_NYMD)
            EXISTS_IMPRV_DATA = .FALSE. !reset data-collection flag
         END IF
      END IF
      print*, "IMPRV>> Done imprv forward."
!}}}
      END SUBROUTINE IMPRV_PROFILE_FORWARD
      
      
      SUBROUTINE UPDATE_AEROAVG( BCPI,BCPO,OCPI,OCPO )
!Grab aeorosol data and compute average in ug/m**3
!{{{
!respectively number of lon/lat gridboxes
      USE DAO_MOD,     ONLY : AIRVOL !grid representing air volume
      USE TIME_MOD,    ONLY : GET_TS_CHEM
!function to fetch chemistry time step
      
! Arguments
      REAL*8,  INTENT(IN)   :: BCPI(IIPAR,JJPAR)
      REAL*8,  INTENT(IN)   :: BCPO(IIPAR,JJPAR)
      REAL*8,  INTENT(IN)   :: OCPI(IIPAR,JJPAR)
      REAL*8,  INTENT(IN)   :: OCPO(IIPAR,JJPAR)
      
! Local variables
      REAL*8                :: CONV_FACTOR, UNITS_FACTOR
      INTEGER               :: I,J,K
      
      
!     Conversion Factor
!     STT : KG of OC/BC in a particular grid cell.
!     IMPROVE : daily average in ug/m^3
!     Conversion:  
!     * 1e9 (kg -> ug)
!     / AIRVOL(i,j,1) (ammount -> concentration)
!     * GET_TS_CHEM / (1440do) (ammount -> part of daily average)
!     Units are uniform and taken care of here; AIRVOL dependson
!     grid-cell, and is applied to each one individually.
      
      UNITS_FACTOR = GET_TS_CHEM() / (24.0 * 60.0) * 1d9
      
!     Update carbon averages, and convert from kg/box to ug/m3
      DO K = 1,num_active_cells
         i = profile_indices(1,K)
         j = profile_indices(2,K)
         CONV_FACTOR = UNITS_FACTOR / AIRVOL(i,j,1)

         avg_bcpi(K) = avg_bcpi(K) + bcpi(i,j) * CONV_FACTOR
         avg_bcpo(K) = avg_bcpo(K) + bcpo(i,j) * CONV_FACTOR
         avg_ocpi(K) = avg_ocpi(K) + ocpi(i,j) * CONV_FACTOR
         avg_ocpo(K) = avg_ocpo(K) + ocpo(i,j) * CONV_FACTOR
      ENDDO
      
!}}}
      END SUBROUTINE UPDATE_AEROAVG
      
      
      SUBROUTINE WRITE_AEROAVG( YYYYMMDD)
!write aeroavg to file and reinitialize
!{{{
      USE TIME_MOD, ONLY: EXPAND_DATE
      
      INTEGER, INTENT(IN) :: YYYYMMDD
      INTEGER :: hms_dummy
      CHARACTER(LEN=64) :: FILENAME
      INTEGER :: K, FILE_UNIT=73
      
!construct unique filename, open for writing
      FILENAME = TRIM(temp_directory) // "aeroavg.YYYYMMDD"
      CALL EXPAND_DATE(FILENAME, YYYYMMDD, hms_dummy)
      FILENAME = TRIM(FILENAME)
      print*, "IMPRV>> Writing aeroavg data to: ", FILENAME
      print*, "IMPRV>> Writing ", num_active_cells, " lines."
      
      OPEN(UNIT=FILE_UNIT, FILE=FILENAME)
      DO K=1,num_active_cells
         WRITE(FILE_UNIT, "(ES12.6,X,E12.6,X,E12.6,X,E12.6)")
     &        avg_bcpi(K), avg_bcpo(K), avg_ocpi(K), avg_ocpo(K)
      END DO
      
!close file
      CLOSE(FILE_UNIT)
      
!reset averages
      avg_bcpi = 0.0d0
      avg_bcpo = 0.0d0
      avg_ocpi = 0.0d0
      avg_ocpo = 0.0d0
      
!}}}
      END SUBROUTINE WRITE_AEROAVG
      
      
      
!During reverse======================================================
      
      SUBROUTINE IMPRV_PROFILE_REVERSE(COST_FUNC)
!Reverse logic and control.  Note that delta-time is negative in
!this context.
!{{{
      USE TIME_MOD, ONLY: GET_NYMD, GET_HOUR, GET_MINUTE, GET_TS_CHEM
      USE TIME_MOD, ONLY: GET_NHMS, GET_NHMSB, EXPAND_DATE
      
      INTEGER :: tmp_NYMD, tmp_hhmmss, THRESHOLD
      INTEGER :: MINUTES_TO_MIDNIGHT, MINUTES_SINCE MIDNIGHT
      INTEGER :: I,J,K
      CHARACTER(LEN=128) :: temp_filename
      
      
      tmp_NYMD = get_nymd()
      tmp_hhmmss = get_nhms()
      print*, "IMPRV>> Starting imprv_profile_reverse @"
      print*, "IMPRV>>   ", tmp_NYMD, get_hour(), get_minute()
      
      MINS_SINCE_MIDNIGHT = 60*(23-GET_HOUR()) + (60-GET_MINUTE())
      MINS_TO_MIDNIGHT = 60*GET_HOUR() + GET_MINUTE()
      THRESHOLD = GET_TS_CHEM()

!check to see if improve data exists when we start a new day
      IF (MINS_SINCE_MIDNIGHT .le. THRESHOLD) THEN
         print*, "IMPRV>> Checking for aeroavg"
         exists_oc_data = .false.
         exists_bc_data = .false.
         CALL GET_IMPRV_BC_DATA(GET_NYMD(), tmp_imprv_data)
         IF (any(tmp_imprv_data>0)) EXISTS_BC_DATA = .true.
         CALL GET_IMPRV_OC_DATA(GET_NYMD(), tmp_imprv_data)
         IF (any(tmp_imprv_data>0)) EXISTS_OC_DATA = .true.
         EXISTS_IMPRV_DATA = EXISTS_OC_DATA .or. EXISTS_BC_DATA
         IF (EXISTS_IMPRV_DATA) THEN
            call read_aeroavg(tmp_NYMD)
            print*, "IMPRV>> Reading aeroavg and Assimilating"
         ENDIF
      ENDIF
      
!Iff we've got improve data this day, calculate cost function
      IF (exists_imprv_data) THEN
         IF (tmp_hhmmss == 230000) THEN
            print*, "IMPRV>> Calculating CF and forcing."
            CALL CALC_IMPRV_COST(COST_FUNC)
         ELSE
            print*, "IMPRV>> Done assimilation for this day"
         ENDIF

         IF (MINS_TO_MIDNIGHT .lt. THRESHOLD ) THEN
!if the day is done, stop assimilating data
            print*, "IMPRV>> Done assimilation for ", tmp_NYMD
            exists_imprv_data = .FALSE. 
         END IF
      END IF
      print*, "IMPRV>> Done imprv_profile_reverse."
      
#ifdef imprv_finite_diff
!     write cf_by_cell to file if we're approaching the start-date
      IF (GET_NYMD() == START_DATE) THEN
!     if date = first date of run
         IF (MOD(GET_NHMS()-GET_NHMSB(), 1000) <= 2) THEN
            temp_filename = TRIM("cf_by_cell.YYYYMMDD")
            CALL EXPAND_DATE(temp_filename, get_nymd(), get_nhms())
            temp_filename = TRIM(temp_filename)
            print*, "IMPRV>> Writing cf_by_cell to ", temp_filename
            print*, "IMPRV>> num_active_cells = ", num_active_cells
            print*, cost_func_by_cell
            OPEN(UNIT=81, FILE=temp_filename)
            DO K=1, num_active_cells
               I = profile_indices(1,k)
               J = profile_indices(2,k)
               WRITE(81, "(I4,I4,F16.8)")
     &              I, J, cost_func_by_cell(k)
            ENDDO
            CLOSE(81)
         ENDIF
      ENDIF
#endif
      END SUBROUTINE IMPRV_PROFILE_REVERSE
      
      
      
      SUBROUTINE READ_AEROAVG(YYYYMMDD)
!Load model data saved during forward run 
      USE TIME_MOD, ONLY: EXPAND_DATE
      INTEGER, INTENT(IN) :: YYYYMMDD
      INTEGER :: hms_dummy, K, file_unit=73
      CHARACTER :: filename*64
      
!Construct filename, open for reading
      filename = TRIM(temp_directory) // "aeroavg.YYYYMMDD"
      CALL EXPAND_DATE(filename, YYYYMMDD, hms_dummy)
      print*, "IMPRV>> Reading saved data from: ", trim(filename)
      OPEN(unit=file_unit, file=filename)
      
!Read values
      DO k=1,num_active_cells
         READ(FILE_UNIT,  "(ES12.6,X,E12.6,X,E12.6,X,E12.6)")
     &        avg_bcpi(K), avg_bcpo(K), avg_ocpi(K), avg_ocpo(K)
         print*, "IMPRV>>   (k,avg_bcpi(k),avg_bcpo(k))=", 
     &        k, avg_bcpi(k), avg_bcpo(k)
         print*, "IMPRV>>   (k,avg_ocpi(k),avg_ocpo(k))=", 
     &        k, avg_ocpi(k), avg_ocpo(k)
      END DO
      
      CLOSE(FILE_UNIT)
      END SUBROUTINE READ_AEROAVG
      

      
      !== COST_FUNC helper functions
      FUNCTION DELTA_COST_FUNC(cpo, cpi, imprv_val, imprv_err)
     $     RESULT (dCF)
!-- Change in cost function at each timestep. cpo and cpi are *average*
!--   values calculated during forward run (24-hour period).
      REAL*4 :: cpo, cpi, imprv_val, imprv_err
      REAL*4 :: dCF

      dCF = 0.5 * 1.0/(imprv_err**2 + IMPRV_ABS_ERR) * 
     $     (1.0/imprv_val)**2 *
     $     ((cpo+cpi) - imprv_val)**2
      END FUNCTION DELTA_COST_FUNC


      FUNCTION DELTA_ADJOINT_FORCING(cpo, cpi, imprv_val, imprv_err,
     $     airvol) RESULT(ADJ_FORCE)
      ! adjoint forcing at each step; args are as in DELTA_COST_FUNC
      REAL*4 :: cpo, cpi, imprv_val, imprv_err
      REAL*8 :: airvol
      REAL*4 :: ADJ_FORCE

      ADJ_FORCE = (1.0/imprv_val)**2 *
     $     1.0/(imprv_err**2 + IMPRV_ABS_ERR) *
     $     (cpo+cpi - imprv_val) * 1e9 / airvol ! * day_step_ratio / day_step_ratio
! * and / by day-step-ratio to make units/frequency of calculation
!     work out, but we'll cancel that to avoid catastrophic divisions
!     and such (just in case the compiler doesn't catch it.
      END FUNCTION DELTA_ADJOINT_FORCING


      
      SUBROUTINE CALC_IMPRV_COST(COST_FUNC)
!Update the cost function, forcing and adjoint for IMROVE assimilation.
      USE ERROR_MOD, ONLY: IT_IS_NAN, ERROR_STOP
      USE CHECKPT_MOD, ONLY: CHK_STT
      USE TIME_MOD, ONLY: GET_TS_CHEM, EXPAND_DATE
      USE TIME_MOD, ONLY: GET_NYMD, GET_NYMDB
      USE TIME_MOD, ONLY: GET_NHMS, GET_NHMSB
      USE ADJ_ARRAYS_MOD, ONLY: STT_ADJ, ADJ_FORCE
      USE DAO_MOD, ONLY: AIRVOL
      
      IMPLICIT NONE
      
!Arguments
      REAL*8 :: COST_FUNC
!Parameters
!Local variables
      INTEGER :: I,J,K
      INTEGER :: yyyymmdd, hhmmss, logfile_handle=235
      
      
!Subroutine calc_imprv_force begins here
      yyyymmdd = GET_NYMD()
      hhmmss = GET_NHMS()
      print*, "IMPRV>> Calculating IMPRV cost @"
      print*, "    ", yyyymmdd, hhmmss
      print*, "    with ", num_active_cells, " active cells"
      
      
      CALL get_imprv_bc_data(yyyymmdd, imprv_bc_data)
      CALL get_imprv_oc_data(yyyymmdd, imprv_oc_data)
      
      print*, "cell, model_bc, model_oc, imprv_bc, imprv_oc"
      DO K=1, num_active_cells
         print*, k, avg_bcpi(k)+avg_bcpo(k), avg_ocpi(k)+avg_ocpo(k), 
     &        imprv_bc_data(1, k), imprv_oc_data(1, k)
      ENDDO
      
!Calculate forcing

!--   ADJ_FORCE is dJ/du where u is the concentration of the
!     tracers on which J depends __in kg per box__.  As such, some
!     unit conversions are necessary. STT_ADJ tracks contributions to
!     cost function from different tracers, and should 
      
!     Contributions to cost function from each grid-cell with IMPROVE
!     data.


!     Black Carbon
      dCF = 0.0
!     $OMP PARALLEL DO
!     $OMP+DEFAULT( SHARED )
!     $OMP+PRIVATE(K, I, J)
      DO K=1, num_active_cells
         I = profile_indices(1,k)
         J = profile_indices(2,k)

         IF (imprv_bc_data(1,K) >= 0.0 .AND.
     $        imprv_bc_data(2,k) >= 0.0) THEN
            IF (hhmmss .eq. 230000) THEN
               dCF(k) = DELTA_COST_FUNC(avg_bcpi(k), avg_bcpo(k),
     $              imprv_bc_data(1,k), imprv_bc_data(2,k))
               
               COST_FUNC_BY_CELL(k) = COST_FUNC_BY_CELL(k) + dCF(k)

               ADJ_FORCE(I,J,1, IDTBCPI) =
     $              DELTA_ADJOINT_FORCING(avg_bcpi(k), avg_bcpo(k),
     $              imprv_bc_data(1,k), imprv_bc_data(2,k),
     $              AIRVOL(I,J,1)) 

               ADJ_FORCE(I,J,1, IDTBCPO) =
     $              DELTA_ADJOINT_FORCING(avg_bcpi(k), avg_bcpo(k),
     $              imprv_bc_data(1,k), imprv_bc_data(2,k),
     $              AIRVOL(I,J,1)) 

               print*, "adj_force(bcpi)", i, j, adj_force(I,J,1,idtbcpi)
               print*, "  made of ", imprv_bc_data(1,k),
     $              imprv_bc_data(2,k), avg_bcpi(k), avg_bcpo(k),
     $               AIRVOL(i,j,1)

               STT_ADJ(I,J,1,IDTBCPI) = STT_ADJ(I,J,1,IDTBCPI) + 
     &              ADJ_FORCE(I,J,1,IDTBCPI)
               STT_ADJ(I,J,1,IDTBCPO) = STT_ADJ(I,J,1,IDTBCPO) + 
     &              ADJ_FORCE(I,J,1,IDTBCPO)
            ENDIF
         ENDIF
      ENDDO
!     $OMP END PARALLEL DO
      print*, "dCF BC @", yyyymmdd, " : ", sum(dCF)
      COST_FUNC = COST_FUNC + sum(dCF)

         
!     Organic Carbon
      dCF = 0.0   
!     $OMP PARALLEL DO
!     $OMP+DEFAULT( SHARED )
!     $OMP+PRIVATE(K, I, J)
      DO K=1, num_active_cells
         I = profile_indices(1,k)
         J = profile_indices(2,k)
         IF (imprv_oc_data(1,k) >=  0.0 .AND.
     $        imprv_oc_data(2,k) >= 0.0) THEN
            IF (hhmmss .eq. 230000) THEN
               dCF(k) = DELTA_COST_FUNC(avg_ocpi(k), avg_ocpo(k),
     $              imprv_oc_data(1,k), imprv_oc_data(2,k))

               COST_FUNC_BY_CELL(k) = COST_FUNC_BY_CELL(k) + dCF(k)

               ADJ_FORCE(I,J,1,IDTOCPI) = DELTA_ADJOINT_FORCING(
     $              avg_ocpi(k), avg_ocpo(k), imprv_oc_data(1,k),
     $              imprv_oc_data(2,k), AIRVOL(I,J,1))

               ADJ_FORCE(I,J,1,IDTOCPO) = DELTA_ADJOINT_FORCING(
     $              avg_ocpi(k), avg_ocpo(k), imprv_oc_data(1,k),
     $              imprv_oc_data(2,k), AIRVOL(I,J,1))
               
               STT_ADJ(I,J,1,IDTOCPI) = STT_ADJ(I,J,1,IDTOCPI) + 
     &              ADJ_FORCE(I,J,1,IDTOCPI)
               STT_ADJ(I,J,1,IDTOCPO) = STT_ADJ(I,J,1,IDTOCPO) + 
     &              ADJ_FORCE(I,J,1,IDTOCPO)
            ENDIF
         ENDIF
      ENDDO
!     $OMP END PARALLEL DO
      print*, "dCF OC @", yyyymmdd, " : ", sum(dCF)
      COST_FUNC = COST_FUNC + sum(dCF)
      dCF = 0.0


      
      IF (IT_IS_NAN(COST_FUNC)) THEN
         CALL ERROR_STOP("Cost Function is NaN", 'calc_imprv_cost')
      ENDIF
      
      print*, "IMPRV>> Done calc_imprv_cost"
      END SUBROUTINE calc_imprv_cost
      
      
      
      
      
!Cleanup=============================================================
      
      SUBROUTINE CLEANUP_IMPRV_PROFILE()

      IF (ALLOCATED(avg_bcpi)) DEALLOCATE(avg_bcpi)
      IF (ALLOCATED(avg_bcpo)) DEALLOCATE(avg_bcpo)
      IF (ALLOCATED(avg_ocpi)) DEALLOCATE(avg_ocpi)
      IF (ALLOCATED(avg_ocpo)) DEALLOCATE(avg_ocpo)
      IF (ALLOCATED(cost_func_by_cell)) DEALLOCATE(cost_func_by_cell)
      IF (ALLOCATED(profile_indices)) DEALLOCATE(profile_indices)
      IF (ALLOCATED(imprv_bc_profile)) DEALLOCATE(imprv_bc_profile)
      IF (ALLOCATED(imprv_oc_profile)) DEALLOCATE(imprv_oc_profile)
      IF (ALLOCATED(imprv_bc_data)) DEALLOCATE(imprv_bc_data)
      IF (ALLOCATED(imprv_oc_data)) DEALLOCATE(imprv_oc_data)
      IF (ALLOCATED(imprv_bc_data)) DEALLOCATE(imprv_bc_data)
      IF (ALLOCATED(imprv_oc_data)) DEALLOCATE(imprv_oc_data)
      IF (ALLOCATED(dCF)) DEALLOCATE(dCF)

      END SUBROUTINE CLEANUP_IMPRV_PROFILE
      
      
!End ================================================================
      END MODULE IMPRV_PROFILE_MOD
      
!###############################################################################
!###############################################################################


!      PROGRAM test
!        USE IMPRV_PROFILE_MOD
!        IMPLICIT NONE
!
!        REAL*8 :: cf
!
!        call imprv_profile_init()
!
!        call imprv_profile_forward(V
!
!        call imprv_profile_reverse()
!
!      END PROGRAM test
